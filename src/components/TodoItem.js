import React, { Component } from 'react';
import  ProfTypes from "prop-types";

export class TodoItem extends Component {
    getStyle = () => {
        return {
            background : "#f3f3f3",
            padding : "10px",
            borderBottom : "1px #ccc solid",
            textDecoration : this.props.todo.completed ? 'line-through' : 'none' ,
        }
    }


    render() {
        const { id, title} = this.props.todo;

        return (
            <div style={this.getStyle()}>
                <p>
                <input type="checkbox" onChange={this.props.markComplete.bind(this, id)}/> {' '}
                { title }
                <button onClick={this.props.delTodo.bind(this, id)} style={btnStyle}>x</button>
                </p>
            </div>
        )
    }
}

TodoItem.ProfTypes = {
    todos : ProfTypes.object.isRequired,
    markComplete : ProfTypes.func.isRequired,
    delTodo : ProfTypes.func.isRequired,
}

const btnStyle = {
    background : "#ff0000",
    color : "#fff",
    border : "none",
    padding : "5px 9px",
    borderRadius : "50%",
    cursor : "pointer",
    float : "right",
}


export default TodoItem;
