import React from 'react'

function About() {
    return (
        <React.Fragment>
            <h1 style={{ textAlign:"center" , marginTop:"10px"}}>About</h1>
            <p>Hi, I am Mitul.</p>
            <p style={ marginTop}>This is my first react app. I will try to learn react and react-native for android and web both.</p>
            <p><span style={SpanStyle}>My Skill :</span> Html, Css, Bootstrap, JavaScript, PHP, MySql, Laravel</p>
            <p><span style={SpanStyle}>DevTools :</span> Git,Phpstrom,Visual Studio, Trello Board etc</p>
            <p><span style={SpanStyle}>Learing : </span> React, React-native, Python, Linux</p>
            <p><span style={SpanStyle}>Summary : </span> Keep Prayer for my programming journy.</p>
            <p style={ marginTop}>Regards,</p>
            <p style={{ fontStyle:"italic" }}>Ikhtiar Mitul</p>
        </React.Fragment>
    )
}

const SpanStyle = {
    color : "Grey",
    fontWeight : "bold",
    marginTop : "5px",
}
const marginTop = {
    marginTop : "20px"
} 
export default About;
