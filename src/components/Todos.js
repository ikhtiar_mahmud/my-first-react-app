import React, { Component } from 'react';
import TodoItem from "./TodoItem";
import  ProfTypes from "prop-types";

class Todos extends Component {
    

  render(){
    return this.props.todos.map((todo) => (
       <TodoItem key={todo.id} todo={todo} markComplete={this.props.markComplete} delTodo={this.props.delTodo}/>
    ));
  }
}
 Todos.ProfTypes = {
     todos : ProfTypes.array.isRequired,
     markComplete : ProfTypes.func.isRequired,
     delTodo : ProfTypes.func.isRequired,
 }

export default Todos;