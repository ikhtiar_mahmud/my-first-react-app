import React, { Component } from 'react';
import { BrowserRouter as Router, Route} from 'react-router-dom';
import Todos from "./components/Todos";
import AddTodo from "./components/AddTodo";
import About from "./components/pages/About";
import Header from './components/layout/Header';
import uuid from "uuid";
// import axios from 'axios'; //this is for api
import './App.css';
class App extends Component {
  state = {
    todos : [
      // {
      //   id : uuid.v4(),
      //   title : "Bangladesh is beatiful",
      //   completed : false,
      // },
      // {
      //   id : uuid.v4(),
      //   title : "Dinner with mom",
      //   completed : false,
      // },
      // {
      //   id : uuid.v4(),
      //   title : "Meeting with boss",
      //   completed : false,
      // }
    ]
  }

  // componentDidMount(){
  //   axios.get('https://jsonplaceholder.typicode.com/todos?_limit=10')
  //        .then(res => this.setState({  todos : res.data }));
  // }

  // Toggole Complete Value
  markComplete = (id) => {
    this.setState( { todos : this.state.todos.map(todo => {
      if(todo.id === id){
        todo.completed = !todo.completed
      }
      return todo;
    }) });
    }

  //Delete Todo
  delTodo = (id) => {
    // axios.delete(`https://jsonplaceholder.typicode.com/todos/${id}`)
    // .then(res => this.setState({ todos : [...this.state.todos.filter(todo => todo.id !== id)]})) //this is for api call
    this.setState({ todos : [...this.state.todos.filter(todo => todo.id !== id)]});

  }

  //addTodo

  addTodo = (title) => {
      // axios.post('https://jsonplaceholder.typicode.com/todos',{
      //   title,
      //   completed : false,
      // }).then(res => this.setState({ todos : [...this.state.todos, res.data]})); //this is for api call
      const newTodo = {
        id : uuid.v4(),
        title,
        completed : false,
      }
      this.setState({ todos : [...this.state.todos, newTodo]});
  }
  

  render(){
    return (
       <Router>
          <div className="App">
          <div className="container">
          <Header />
          <Route exact path="/" render={ props => (
            <React.Fragment>
                <AddTodo addTodo={this.addTodo}/>
                <Todos todos={this.state.todos} markComplete={this.markComplete} delTodo={this.delTodo}/>
            </React.Fragment>
            )} />
          <Route path="/about" component={About}/>
          </div>
          </div>
       </Router>
    );
  }
}

export default App;
